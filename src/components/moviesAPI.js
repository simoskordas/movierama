/*jslint node:true */
'use strict';

var RestClient = require('restler');
var Promise = require('promise');
var moment = require('moment');
var appLogger = require('../core/logging').appLogger;
var Bottleneck = require("bottleneck");

function MoviesAPI () { }

MoviesAPI.prototype.getUrl = function (url) { 
    var restClient = RestClient;
    var that = this;
    var apiPromise =  new Promise(function (resolve, reject) {
        restClient.get(url, {timeout:5000, parser: restClient.parsers.json})
            .on('complete', function(result) {
                if (result instanceof Error) {
                    appLogger.info(that.name = ':getUrl: Failed : ' + result);
                    reject('Failed');
                } else {
                    appLogger.info(that.name + ':getUrl: success');
                    resolve(result);
                }
            });          
    });  
    return apiPromise;
};  
MoviesAPI.prototype.getCommonMovieModels = function (apiResponse) {
    appLogger.info(this.name + ':getCommonMovieModels');
    var apiMoviesList = this.extractMoviesListFromAPIResults(apiResponse);
    var toCommonMovieModelBinded = (this.toCommonMovieModel).bind(this);
    var moviesListCurrentItem = 0;
    var promisesArray = [];
    var totalMovieObjectsNumber = apiMoviesList.length;
    var createNewPromiseTimer = setInterval(function() {
        if (moviesListCurrentItem == totalMovieObjectsNumber) {
            clearInterval(createNewPromiseTimer);
        }
        else{
            var currentMovieObject = apiMoviesList[moviesListCurrentItem];
            var processingPromise = toCommonMovieModelBinded(currentMovieObject);
            promisesArray.push(processingPromise);
            moviesListCurrentItem++;            
        }
    }, 500);
    
    var allPromisesCompleted = new Promise(function(resolve, reject){
        var processingPromisesArray = promisesArray;
        var allPromisesNumber = totalMovieObjectsNumber;
        var resolveAllProcessesCompleted = resolve;
        var checkForAllPromisesCreatedTimer = setInterval(function() {
            var resolveAllProcessesCompletedInner = resolveAllProcessesCompleted;
            if (processingPromisesArray.length == allPromisesNumber) {
                clearInterval(checkForAllPromisesCreatedTimer);
                Promise.all(processingPromisesArray)
                        .then(function(commonMovieModelsList){
                            appLogger.info('getCommonMovieModels: all promises are done');
                          resolveAllProcessesCompletedInner(commonMovieModelsList);
                        }).catch(function(error){
                          appLogger.error('getCommonMovieModels:  batch promises error: ' + error);
                        });  
            }
        }, 500);             
    });
    return allPromisesCompleted;        
};

MoviesAPI.prototype.getNowPlayingMovies = function () {
    var apiUrl = this.getNowPlayingUrl();
    var that = this;
    var getUrlBinded = this.getUrl.bind(this);    
    var getCommonMovieModelsBinded = this.getCommonMovieModels.bind(this);
    var getMoviesPromise = new Promise(function (resolve, reject) {
        getUrlBinded(apiUrl)
            .then(getCommonMovieModelsBinded)
            .then(function (listOfCommonMovieModels){
                for (var key in listOfCommonMovieModels) {
                    listOfCommonMovieModels[key]['nowPlaying'] = true;
                }
                appLogger.info(that.name + ':returning all commonMovieModels');
                resolve(listOfCommonMovieModels);
            })
            .catch(function(error){
                appLogger.error(that.name + ':getNowPlayingMovies error: ' + error);
            });
    });          
    return getMoviesPromise;
};
MoviesAPI.prototype.searchMovies = function (searchText) {
    var that = this;    
    var apiUrl = this.searchMoviesUrl(searchText);
    appLogger.info(that.name + ': apiUrl : ' + apiUrl);

    var getUrlBinded = this.getUrl.bind(this);    
    var getCommonMovieModelsBinded = this.getCommonMovieModels.bind(this);
    var getMoviesPromise = new Promise(function (resolve, reject) {
        getUrlBinded(apiUrl)
            .then(getCommonMovieModelsBinded)
            .then(function (listOfCommonMovieModels){
                appLogger.info(that.name + ':returning all commonMovieModels');
                resolve(listOfCommonMovieModels);
            })
            .catch(function(error){
                appLogger.error(that.name + ':searchMovies error: ' + error);
            });
    });          
    return getMoviesPromise;    
};    
MoviesAPI.prototype.isDateValid = function (date) {
    var reg = /^([0-9]{4}-(0[1-9]|1[0-2])-([0-2]{1}[0-9]{1}|3[0-1]{1}))$/
    return date.match(reg)
};


module.exports = MoviesAPI;