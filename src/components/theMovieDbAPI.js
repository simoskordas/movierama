/*jslint node:true */
'use strict';

var RestClient = require('restler');
var Promise = require('promise');
var moment = require('moment');
var _ = require('underscore');
var appLogger = require('../core/logging').appLogger;
var MoviesAPI = require('./moviesAPI');
var Mustache = require('mustache');


function TheMovieDBAPI () { 
    this.name = 'TheMovieDBAPI';
    this.key = '186b266209c2da50f898b7977e2a44dd';
    this.nowPlayingURL = 'http://api.themoviedb.org/3/movie/now_playing?api_key={{ apiKey }}';
    //this.nowPlayingURL = 'http://localhost:5000/moviedbList';
    this.movieSearchURL = 'http://api.themoviedb.org/3/search/movie/?query={{ searchText }}&api_key={{ apiKey }}';
    this.movieDetailsURL = 'http://api.themoviedb.org/3/movie/{{ movieID }}?api_key={{ apiKey }}'
};
TheMovieDBAPI.prototype = new MoviesAPI();
var TheMovieDBAPIObject = new TheMovieDBAPI();

TheMovieDBAPI.prototype.extractMoviesListFromAPIResults = function (apiResponse) { 
    return apiResponse.results || [];
};
TheMovieDBAPI.prototype.getNowPlayingUrl = function () { 
    var url = Mustache.render(this.nowPlayingURL, {
        apiKey: this.key
    });   
    return url;
};
TheMovieDBAPI.prototype.searchMoviesUrl = function (searchText) { 
    var url = Mustache.render(this.movieSearchURL, {
      apiKey: this.key,
      searchText: searchText    
    });   
    return url; 
};
TheMovieDBAPI.prototype.movieDetailsUrl = function (movieID) { 
    var url = Mustache.render(this.movieDetailsURL, {
      apiKey: this.key,
      movieID: movieID    
    });   
    return url; 
};
TheMovieDBAPI.prototype.toCommonMovieModel = function (apiMovieObject) { 
    var that = this;
    var restClient = RestClient;
    var promise = new Promise(function (resolve, reject) {
        appLogger.info(that.name + ':toCommonMovieModel: ' + apiMovieObject.title);

        var releaseYearStr = apiMovieObject.release_date.substring(0, 4);
        var year = parseInt(releaseYearStr);
        var commonMovieObject = {  
              title: apiMovieObject.title,
              description: apiMovieObject.overview,
              year: year,
              releaseDate: apiMovieObject.release_date,
              reviewsNumberRotten: 0,
              reviewsNumberTheMovieDB: apiMovieObject.vote_count,
              nowPlaying: false,            
              actors: []
            };
        resolve(commonMovieObject); 
    });
    return promise;      
};



module.exports = TheMovieDBAPIObject;