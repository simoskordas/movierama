/*jslint node:true */
'use strict';

var RestClient = require('restler');
var Promise = require('promise');
var moment = require('moment');
var _ = require('underscore');
var appLogger = require('../core/logging').appLogger;
var MoviesAPI = require('./moviesAPI');
var Mustache = require('mustache');
var async = require('async');


function RottenTomatoesAPI () { 
    this.name = 'RottenTomatoesAPI';
    this.key = 'qtqep7qydngcc7grk4r4hyd9';
    this.nowPlayingURL = 'http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?apikey={{ apiKey }}';
    //this.nowPlayingURL = 'http://localhost:5000/rottenList';
    this.movieReviewsURL = 'http://api.rottentomatoes.com/api/public/v1.0/movies/{{ movieID }}/reviews.json?apikey={{ apiKey }}';
    //this.movieReviewsURL = 'http://localhost:5000/rottenQueryReviews';
    this.movieSearchURL = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?q={{ searctText }}&page_limit=50&page=1&apikey={{ apiKey }}'
};
RottenTomatoesAPI.prototype = new MoviesAPI();
var RottenTomatoesAPIObject = new RottenTomatoesAPI();

RottenTomatoesAPI.prototype.extractMoviesListFromAPIResults = function (apiResponse) { 
    return apiResponse.movies || [];
};
RottenTomatoesAPI.prototype.getNowPlayingUrl = function () { 
    var url = Mustache.render(this.nowPlayingURL, {
      apiKey: this.key
    });   
    //appLogger.info(this.name + ':getNowPlayingUrl: ' + url);
    return url;
};
RottenTomatoesAPI.prototype.getMovieReviewsUrl = function (movieID) { 
    var url = Mustache.render(this.movieReviewsURL, {
      apiKey: this.key,
      movieID: movieID    
    });   
    //appLogger.info(this.name + ':getMovieReviewsUrl: ' + url);
    return url; 
};
RottenTomatoesAPI.prototype.searchMoviesUrl = function (searctText) { 
    var url = Mustache.render(this.movieSearchURL, {
      apiKey: this.key,
      searctText: searctText    
    });   
    return url; 
};
RottenTomatoesAPI.prototype.getMovieReviewsNumber = function (movieID) { 
   var that = this;
   var restClient = RestClient;
   var reviewsUrl = that.getMovieReviewsUrl(movieID);    
   var promise = new Promise(function (resolve, reject) {
      restClient.get(reviewsUrl, {timeout:5000, parser: restClient.parsers.json}).on('complete', function(result) {
         if (result instanceof Error) {
            appLogger.info(that.name + ':getMovieReviewsNumber: Failed: ' + result);
            resolve(0);
         } 
         else {
            appLogger.info(that.name + ':getMovieReviewsNumber: retrieved reviews number: ' + result.total);
            resolve(result.total);
         }
      });    
   });
   return promise;
};    
RottenTomatoesAPI.prototype.toCommonMovieModel = function (apiMovieObject) { 
    var that = this;
    var restClient = RestClient;
    
    var promise = new Promise(function(resolve, reject){
        that.getMovieReviewsNumber(apiMovieObject.id).then(function(movieReviewsNumber) {
            var movieObject = apiMovieObject;
            appLogger.info(that.name + ':toCommonMovieModel: ' + movieObject.title);
            var actors = movieObject.abridged_cast.map(function(actor){
                return actor.name;
            });
            var commonMovieObject = {  
                  title: movieObject.title,
                  description: movieObject.synopsis,
                  year: movieObject.year,
                  releaseDate: movieObject.release_dates.theater,
                  reviewsNumberRotten: movieReviewsNumber,
                  reviewsNumberTheMovieDB: 0,
                  nowPlaying: false,
                  actors: actors
                }; 
            resolve(commonMovieObject);
        });    
    });
    return promise;      
};


module.exports = RottenTomatoesAPIObject;