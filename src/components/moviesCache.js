/*jslint node:true */
'use strict';

var MoviesModel = require('../model/movies');
var RestClient = require('restler');
var Promise = require('promise');
var PromiseThrottle = require('promise-throttle');
var appLogger = require('../core/logging').appLogger;
var moment = require('moment');
var _ = require('underscore');
var mongoose = require('mongoose');
var RottenTomatoesAPI = require('./rottenTomatoesAPI');
var TheMovieDbAPI = require('./theMovieDbAPI');
var MoviesAPI = require('./moviesAPI');
var Movie = require('../model/movies');
var async = require('async');
var cron = require('cron');


function MoviesCache (test) {
    this.name = 'MoviesCache';
    this.sources = {
        'ROTTEN': {
            apiClient: RottenTomatoesAPI
        },
        'THE_MOVIE_DB': {
            apiClient: TheMovieDbAPI
        }
    };
    this.cronExpresion = '1 */12 * * *';
    this.removeOlderThanDays= 7;
}

MoviesCache.prototype.startCacheRefresh = function () {
    this.cacheRefresh();
    var cronJob = cron.job(this.cronExpresion, (this.cacheRefresh).bind(this)); 
    cronJob.start();    
};
MoviesCache.prototype.removeOldMovies = function () {
    var today = moment();
    var todayMinus7Days = today.subtract(7, 'days');
     Movie.find({}, function(err, docs){
         var keepInCacheDate = todayMinus7Days;
         docs.forEach(function(doc){
             var date = mongoose.Types.ObjectId(doc.id).getTimestamp()
             var creationDateIsoFormat = JSON.stringify(date);
             var creationDate = new Date(creationDateIsoFormat);
             if (keepInCacheDate.isAfter(creationDate)){
                 appLogger.debug('Removing object');
                 doc.remove();
             }
         });

     });  
};
MoviesCache.prototype.cacheRefresh = function () {
    appLogger.info('refreshCache');    
    var sourceQueries = [];
    var that = this;
    
    this.removeOldMovies();
    Object.keys(this.sources).forEach(function(key){
        var apiClient = that.sources[key]['apiClient'];
        var queryPromise = apiClient.getNowPlayingMovies.apply(apiClient, []); 
        sourceQueries.push(queryPromise);
    })     
    var queryMethod = MoviesAPI.prototype.getNowPlayingMovies;
    return this.queryParallelExecutor(sourceQueries)
            .then(that.updateCache.bind(that))
            .catch(function(error){
        appLogger.error('parallel executor error : ' + error);   
    });
};
MoviesCache.prototype.cacheSearch = function (searchText) {
    appLogger.info('cacheSearch: ' + searchText);    
    var regex = new RegExp('^.*' + searchText + '.*$', 'i');   // (one|two|three)
    
    var promise =  new Promise(function (resolve, reject) {    
        appLogger.info('Cache is searching...');
        var returnResults = resolve;

         Movie.find({'title': regex}, function(err, docs){
             appLogger.info('Found ' + docs.length + ' movies in cache');
             returnResults(docs);
         });    
    });
    return promise;
};
MoviesCache.prototype.getNowPlayingMovies = function (searchText) {
    appLogger.info('getNowPlayingMovies');    
    var promise =  new Promise(function (resolve, reject) {    
        var returnResults = resolve;
         Movie.find({'nowPlaying': 'true'}, (function(err, docs){
             appLogger.info('Found ' + docs.length + ' movies in cache');
             returnResults(docs);
         }));    
    });
    return promise;
};
MoviesCache.prototype.apiSearch = function (searchText) {
    appLogger.info('apiSearch');    
    var sourceQueries = [];
    var that = this;
    Object.keys(this.sources).forEach(function(key){
        var apiClient = that.sources[key]['apiClient'];
        var queryPromise = apiClient.searchMovies.apply(apiClient, [searchText]); 
        sourceQueries.push(queryPromise);
    })     
    var queryMethod = MoviesAPI.prototype.searchMovies;
    return this.queryParallelExecutor(sourceQueries)
            .then(that.updateCache.bind(that))
            .catch(function(error){
        appLogger.error('parallel executor error : ' + error);   
    });
};
MoviesCache.prototype.queryParallelExecutor = function(sourceQueries) {
    var that = this;
    var promise =  new Promise(function (resolve, reject){
        appLogger.info(that.name + ':queryParallelExecutor');
        Promise.all(sourceQueries).then(function (executionResults){
            appLogger.info(that.name + ':queryParallelExecutor done');
            resolve(executionResults)
        }).catch(function(reason) {
           appLogger.error(that.name + ':queryParallelExecutor : ' + reason);
        });    
    }).catch(function(error){
        appLogger.error(that.name + ':queryParallelExecutor: ' + error);
    });  
    return promise;    
};

MoviesCache.prototype.updateMovieObject = function (originalMovie, newMovie) {
    appLogger.debug('updateMovieObject start');
    if ((typeof originalMovie == 'undefined') || Object.keys(originalMovie).length == 0){
        return newMovie;
    }
    var mergeObject = {
          title: newMovie.title,
          description: (originalMovie.description > newMovie.description? originalMovie.description : newMovie.description),
          year: newMovie.year,
          releaseDate: newMovie.releaseDate,
          reviewsNumberRotten: (originalMovie.reviewsNumberRotten > 0 ? originalMovie.reviewsNumberRotten : newMovie.reviewsNumberRotten),
          reviewsNumberTheMovieDB: (originalMovie.reviewsNumberTheMovieDB > 0 ? originalMovie.reviewsNumberTheMovieDB : newMovie.reviewsNumberTheMovieDB),  
          nowPlaying: newMovie.nowPlaying || originalMovie.nowPlaying,
          actors: newMovie.actors.length > originalMovie.actors.length ? newMovie.actors : originalMovie.actors              
    };
    return mergeObject;
}

MoviesCache.prototype.updateCache = function (sourceQueriesArrays) {
    var that = this;
    var serialDBOperations = [];
    var allMovies = [].concat.apply([],sourceQueriesArrays);
    allMovies.forEach(function sourceQueryResult(commonMovieObject) {
        var moviesCache = that;
        serialDBOperations.push((function(callback) {
            var newMovieFromAPI = commonMovieObject;
            var movieDocumentHandler = function(err, movieObjectFromDB) {
                var commonMovieObject = newMovieFromAPI;
                var documentHandlingDoneCallback = callback;

                var removePromise = new Promise(function(resolve, reject){
                    if (err || !movieObjectFromDB) {
                        resolve();
                    }
                    else {
                        appLogger.debug('Found movie: ' + commonMovieObject.title);
                        Movie.remove({'title': movieObjectFromDB.title,'year': movieObjectFromDB.year},function (err) {
                            if (err){
                                appLogger.error('Error removing ' + movieInDB.title);
                            }
                            resolve(movieObjectFromDB);
                        });                      
                    }                            
                }).then(function(movieObjectFromDB){
                    var originalMovieObject = movieObjectFromDB || {};
                    var updatedMovieObject = moviesCache.updateMovieObject(originalMovieObject, newMovieFromAPI);
                    var newMovieDocument = new Movie(updatedMovieObject); 
                    newMovieDocument.save(function(err7, doc, num){
                          documentHandlingDoneCallback();
                    }); 

                }).catch(function(error){appLogger.error(error)});                    
            }
            Movie.findOne({'title': commonMovieObject.title,'year': commonMovieObject.year}, movieDocumentHandler);                
        }).bind(moviesCache));
    });
    async.series(serialDBOperations);    
};

module.exports = new MoviesCache();

