/*jslint node:true */
'use strict';

var path = require('path'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
    http = require('http'),
	express = require('express'),
    app = express(),
    server = http.createServer(app),
    appLogger = require('./core/logging').appLogger;
var config = require('./core/config');
var MoviesCache = require('./components/moviesCache');

// ---- System Initiallize ------
var system = {};
system.config = config;
system['app'] = app;
system['server'] = server;
require('./core/initiallizer')(system);

// ---- App Initiallize ---------
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public',express.static(path.join(__dirname, 'public')));

// Load Application Controllers
require('./controllers')(app);

server.listen(system.config.httpServer.port, function () {
    appLogger.info('App listening at ' + system.config.httpServer.port);
    MoviesCache.startCacheRefresh();
});
