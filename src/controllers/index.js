/*jslint node:true */
'use strict';

var fs = require('fs');
var path = require('path');
var appLogger = require('../core/logging').appLogger;
var db = require('../core/db');

module.exports = function (app) {

  appLogger.info(' ');
  appLogger.info('** Controller loading **');    
    
  fs.readdirSync(__dirname).forEach(function (file) {
    if (file === path.basename(__filename)) { return; }

    appLogger.info('Controller Loader: Loading controller: ' + file);
    require('./' + file)(app);
  });
};