/*jslint node:true */
'use strict';

var appLogger = require('../core/logging').appLogger;
var MoviesCache = require('../components/moviesCache');
var async = require('async');
var _ = require('underscore');
var appLogger = require('../core/logging').appLogger;
var async = require('async');

function listNowPlayingMovies(req, res, next) {
    appLogger.info('Movies: listNowPlayingMovies');
    MoviesCache.getNowPlayingMovies().then(function(results) {
        res.send(results);
    });     
}
function searchMovies(req, res, next) {
    appLogger.info('Movies: searchMovies: ' + req.query.q);
    var serialSearchOperations = [];
    
    var apiSearchPromise = MoviesCache.apiSearch(req.query.q).then(function(noResultsAreReturnedHere){
        appLogger.info('Movies: Apis query ended'); 
        
        // Haven't found the root cause yet, but unless we pause for some millis we get not result
        // for the movies thay are just inserted in the DB....
        setTimeout(function(){
            var cacheSearchPromise = MoviesCache.cacheSearch(req.query.q).then(function(results) {
                res.send(results);
            });                  
        },1000);
        res.send(results);
    });

}
module.exports = function (app) {
    app.get('/api/movies', listNowPlayingMovies);
    app.get('/api/movies/search', searchMovies);
};
