/*jslint node:true */
'use strict';

var config = require('./config'),
    appLogger = require('./logging').appLogger;

module.exports = function (system) {
    
    system.config = config;
    system.modules = {};    
    
    appLogger.info(' ');
    appLogger.info('** Core system initialization **');
    system.modules['applogger'] = appLogger;
    
	var moduleInitListener = function(module, err){
		if(err){
		    appLogger.error('System Initializer: Module ' + module + ': error during initialization: ' + err);
		    appLogger.error('System Initializer: Exiting...');
		    process.exit();        
		}
        appLogger.info('System Initializer: Module ' + module + ' loaded ');
	};  

	['db'].forEach(function (module) {
		appLogger.info('System Initializer: Loading module: ' + module);
		system.modules[module] = require('./' + module)(system);
        system.modules[module].init(moduleInitListener);
	});
};
