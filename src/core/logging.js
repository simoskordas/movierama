/*jslint node:true */
'use strict';

var config = require('./config');
var fs = require('fs');
var winston = require('winston');

fs.mkdir(config.logging.logPath, function(err) {
    if ( err.code != 'EEXIST' ) throw e;
});

var customLevels = {
  levels: {debug: 0, info: 1, warn: 2, error: 3},
  colors: {debug: 'blue', info: 'green', warn: 'yellow', error: 'red'}
};
// create the main logger
if (process.env.NODE_ENV !== 'test') {
    var applogger = new(winston.Logger)({
        level: 'debug',
        levels: customLevels.levels,
        transports: [
            // setup console logging
            new(winston.transports.Console)({
                level: 'info', // Only write logs of info level or higher
                levels: customLevels.levels,
                colorize: true
            }),
            // setup logging to file
            new(winston.transports.File)({
                filename: './logs/system.log',
                maxsize: 1024 * 1024 * 10, // 10MB
                level: 'debug',
                levels: customLevels.levels
            })
        ]
    });
} else {
    var applogger = new(winston.Logger)({
        level: 'debug',
        levels: customLevels.levels,
        transports: [
//            // setup console logging
//            new(winston.transports.Console)({
//                level: 'info', // Only write logs of info level or higher
//                levels: customLevels.levels,
//                colorize: true
//            }),
//            // setup logging to file
//            new(winston.transports.File)({
//                filename: './logs/system.log',
//                maxsize: 1024 * 1024 * 10, // 10MB
//                level: 'debug',
//                levels: customLevels.levels
//            })
        ]
    });
}


winston.addColors(customLevels.colors);

module.exports = {
    'appLogger' : applogger,        
    'init' : function(callback){
        callback('logger');
    }
};



module.export = {
    appLogger: applogger
}