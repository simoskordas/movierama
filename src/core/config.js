/*jslint node:true */
'use strict';

var config = {};

config.httpServer={'hostname':'localhost', 'port':'4000'};
config.mode = 'development'; // development or production
config.db = {'hostname':'localhost', 'port': '27017', 'dbname':'myapp'};
config.logging = {'logPath':'logs'}


module.exports = config;
