/*jslint node:true */
'use strict';

var config = require('./config');
var mongoose = require('mongoose');
var appLogger = require('./logging').appLogger;

module.exports = function(system){

    var module_object = {
        init: function(callback){
            appLogger.info('DB module: subsystem starting up...');

            var db = mongoose.connection;
            db.on('error', function(err){ appLogger.error('Error from Mongo DB: ' + err); });
            db.on('open', function(){ appLogger.info('DB module: connected'); });

            mongoose.connect('mongodb://' + config.db.hostname + ':' + config.db.port + '/' + config.db.dbname , function(err) {
                if (err) {
                    appLogger.error('DB module: error from Mongo DB: ' + err);
                    callback('db', err);
                }
            });

            appLogger.info('DB module: started');                
        },
        disconnect: function(callback) { 
            mongoose.connection.close();
        }
    };
    
    return module_object;
};