/*jslint node:true */
'use strict';

var logging = require('../core/logging'),
    config = require('../core/config');

var logger = logging.getLogger('system');

module.exports = function(app){

    /// catch 404 and forwarding to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // development error handler
    // will print stacktrace
    if (config.mode === 'development') {
        app.use(function(err, req, res, next) {
            logger.error('ERROR HANDLER (Development): ');
            res.status(err.status || 500);
            res.json(err);
             res.render('error', {
                 message: err.message,
                 error: err
             });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
            logger.error('ERROR HANDLER (Production): ');        
        res.status(err.status || 500);
        res.json(err);
         res.render('error', {
             message: err.message,
             error: {}
         });
    });

};








