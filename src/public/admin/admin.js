define('app',['libs','framework','configuration', 'adminTemplate', 'eventModule'],
	          function(Libs, Framework, Configuration, Template, EventModule){

    var Application = new Backbone.Marionette.Application();

    Application.Configuration = Configuration;
    console.log(JSON.stringify(Configuration));
    Application.Models = {};
    Application.Collections = {};
    Application.Views = {};
    Application.Template = {};
    Application.Data = {};

    Application.addRegions({
         main: '#main'
    });

    Application.on("before:start", function(){
        console.log('Application initializing');

        //  ---- Setup Application Modules -------------------------------
        Application.module("Libs", Libs);        
        Application.module("Framework", Framework);  
        Application.module("Template", Template);  
        Application.module("EventModule", EventModule);  
    });


    Application.on("start", function(){
    	Backbone.history.start({ pushState: true });
      	console.log('Application initialization complete');  
    });

    
//    Application.addInitializer(function() {
//       this.initAppLayout();   
//    });                  
//    Application.initAppLayout = function() {
//
//
//        //Application.main.currentView.mainMenu.show(new mainMenuView.Views.menu());
//        //Application.main.currentView.content.show(new dashboard.Views.main());    
//
//       // this can be a main menu navigation
//       // this will change content at the "main" app screen
//       // your links should include the role=nav-main-app
//
//        // $('a[role=nav-main-app]').click(function(e) {
//        //   Application.Router.navigate( $(this).attr('href'), {trigger: true});
//        //   e.preventDefault(); 
//        // });  
//
//    };

	return Application;
});
