require.config({
    baseUrl: '/public/',
    paths: {
        // library modules
        'jquery': 'lib/jquery',
        'underscore': 'lib/underscore',
        'backbone': 'lib/backbone',
        'text' : 'lib/text',
        'handlebars':'lib/handlebars',
        'bootstrap' : 'lib/bootstrap',
        'marionette' : 'lib/backbone.marionette',
        'socketio' : 'lib/socket.io',
        'modernizr' : 'lib/notificationStyles/modernizr.custom',
        'classie' : 'lib/notificationStyles/classie',
        'notificationFx': 'lib/notificationStyles/notificationFx',
        'libs': 'lib/libs',

        // App specific modules
        'eventModel': 'models/eventModel',
        'eventCollection': 'collections/eventCollection',
        'eventView' : 'views/eventView',
        'eventSubmitView' : 'views/eventSubmitView',
        'eventDetailedView':'views/eventDetailedView',        
        'eventCollectionView' : 'views/eventCollectionView',
        'eventModule' : 'modules/eventModule',

        // Framework modules
        'clientEventBus' : 'framework/clientEventBus',
        'serverEventBus' : 'framework/serverEventBus',
        'notifications' : 'framework/notifications',
        'logger' : 'framework/logger',
        'framework' :'framework/framework',
        
        // Template modules
        'adminTemplate': 'templates/admin/adminTemplate',        
        
        // application + corresponding configuration
        'configuration': 'configuration',  
        'router': 'routers/adminRouter',        
        'app': 'admin'        
    },
    shim: {
        'backbone': {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
        },
        'underscore': {
                exports: '_' 
        },
        'handlebars': {
            exports: 'Handlebars'
        },
        'socketio': {
            exports: 'io'
        },
        'modernizr': {
                    exports: 'Modernizr'
                },  
        'classie': {
                    exports: 'classie'
                },           
        'notificationFx': {
                    deps: ['modernizr'],
                    exports: 'NotificationFx'
                }          
        }
});


require(['app','router'], function (Application,Router) {

    Application.addInitializer(function() {
        Application.Router = new Router();
        Application.Router.init();
      });

    Application.start();
    
});