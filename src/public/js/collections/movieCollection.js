define('movieCollection',['backbone', 'configuration','movieModel'],function(Backbone,Configuration,MovieModel){

	var MovieCollection = Backbone.Collection.extend({
		model: MovieModel,
		url: '/api/movies',
	});

	return MovieCollection;
});

