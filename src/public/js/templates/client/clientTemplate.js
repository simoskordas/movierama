define('clientTemplate',
       ['text!templates/client/main.hbs','text!templates/client/mainContent.hbs','text!templates/client/footer.hbs','text!templates/client/header.hbs'],
       function(Main,MainContent,Footer,Header){


	 var ClientTemplate = function(module){
 
         module.Main = Main,
         module.MainContent = MainContent,
         module.Footer = Footer,
         module.Header = Header

	 };

	 return ClientTemplate;

});