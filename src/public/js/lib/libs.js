define('libs',
       ['jquery','underscore','backbone','marionette','handlebars','bootstrap','socketio','modernizr','classie','notificationFx'],
       function($,_,Backbone,Marionette,Handlebars,Bootstrap,SocketIO,Modernizr,classie,NotificationFX){


	 var Libs = function(module){

         module.$ = $,
         module._ = _,             
         module.Backbone = Backbone,
         module.Marionette = Marionette,
         //module.Bootstrap = Bootstrap,             
         module.SocketIO = SocketIO,             
         module.Modernizr = Modernizr,             
         module.classie = classie,             
         module.NotificationFX = NotificationFX                        
	 };

	 return Libs;

});