require.config({
    baseUrl: '/js/',
    paths: {
        // library modules
        'jquery': 'lib/jquery',
        'underscore': 'lib/underscore',
        'backbone': 'lib/backbone',
        'text' : 'lib/text',
        'handlebars':'lib/handlebars',
        'bootstrap' : 'lib/bootstrap',
        'marionette' : 'lib/backbone.marionette',
        'modernizr' : 'lib/notificationStyles/modernizr.custom',

        // App specific modules
        'movieModel': 'models/movieModel',
        'movieCollection': 'collections/movieCollection',
        'movieView' : 'views/movieView',
        'movieCollectionView' : 'views/movieCollectionView',

        // Framework modules        
        'appRouter': 'routers/appRouter',
        
        // Template modules
        'clientLayout': 'templates/clientLayout/clientLayout',
        
        // application + corresponding configuration
        'configuration': 'app/configuration',
        'app': 'app/app'        
    },
    shim: {
        'backbone': {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
        },
        'underscore': {
                exports: '_' 
        },
        'handlebars': {
            exports: 'Handlebars'
        },
        'modernizr': {
                    exports: 'Modernizr'
                }
        }
});


require(['app','appRouter'], function (Application,ApplicationRouter) {

    Application.addInitializer(function() {
        Application.Router = new ApplicationRouter();
        Application.Router.init();
      });

    Application.start();
    
});
