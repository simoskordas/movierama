define('app',['backbone','marionette','handlebars', 'movieModel', 'movieCollection', 'movieView', 'movieCollectionView', 
              'text!templates/client/main.hbs','text!templates/client/header.hbs','text!templates/client/main.hbs','text!templates/client/footer.hbs'],
	    function(Backbone,Marionette,Handlebars,MovieModel,MovieCollection, MovieView, MovieCollectionView, 
	             AppMainTemplate,AppHeaderTemplate,AppMainContentTemplate,AppFooterTemplate){


    var Application = new Backbone.Marionette.Application();

    //Application.Configuration = Configuration;
    Application.Models = {};
    Application.Collections = {};
    Application.Views = {};
    Application.Template = {};
    Application.Data = {};

    Application.addRegions({
         main: '#main'
    });

    Application.on("before:start", function(){
        console.log('Application initializing');

	//  --------------------------------------------------------------
        //  ---- Setup Models ---------------------------------------------
        //  --------------------------------------------------------------
        Application.Models.MovieModel = MovieModel;

        //  --------------------------------------------------------------
        //  ---- Setup Collections ---------------------------------------
        //  --------------------------------------------------------------
        Application.Collections.MovieCollection = MovieCollection;        

        //  --------------------------------------------------------------
        //  ---- Setup Views ---------------------------------------------
        //  --------------------------------------------------------------
        Application.Views.MovieView = MovieView; 
        Application.Views.MovieCollectionView = MovieCollectionView;

        //  -----------------------------------------------------------------
        //  ---- Setup Templates --------------------------------------------
        //  -----------------------------------------------------------------
        var AppMainLayout = Backbone.Marionette.LayoutView.extend({
        	tagName: 'div',
        	id: 'app',
        	className:'app',
        	template: Handlebars.compile(AppMainTemplate),

	        regions: {
	            header: "#headerRegion",
	            mainContent: "#mainContentRegion",
	            footer: "#footerRegion",
	        }

        });
        Application.Template.AppMainLayout = AppMainLayout;

        var AppHeader = Backbone.Marionette.LayoutView.extend({
        	tagName: 'div',
        	id: 'header',
        	className:'header',
        	template: Handlebars.compile(AppHeaderTemplate)
        });
        Application.Template.AppHeader = AppHeader;

        var AppMainContent = Backbone.Marionette.LayoutView.extend({
        	tagName: 'div',
        	id: 'mainContent',
        	className:'mainContent',
        	template: Handlebars.compile(AppMainContentTemplate),

	        regions: {
	            eventsLis: "#eventsLis"
	        }        	
        });     
        Application.Template.AppMainContent = AppMainContent;   

        var AppFooter = Backbone.Marionette.LayoutView.extend({
        	tagName: 'div',
        	id: 'footer',
        	className:'footer',
        	template: Handlebars.compile(AppFooterTemplate)
        });  
        Application.Template.AppFooter = AppFooter;  
    });

    Application.on("start", function(){
    	Backbone.history.start({ pushState: true }); //--Marionette does this automatically?
      	console.log('Application initialization complete');  
    });

    return Application;
});
