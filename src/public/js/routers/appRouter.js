define('appRouter',['backbone','marionette','app','movieCollection', 'movieCollectionView'],
        function(Backbone,Marionette,Application,MovieCollection,MovieCollectionView){

	var App_Router = Backbone.Marionette.AppRouter.extend({
		routes: {
			'':'mainApp'
		},
        onRoute: function(name, path, arguments) {
                // this route is being called. This works
                console.log("on route called", name, path);
            },
		init: function(options) {
			console.log('AppRouter init...');
				
		    //  --------------------------------------------------------------
	        //  ---- Setup Application Data + Data-binded views --------------
	        //  --------------------------------------------------------------        
			Application.Data.Movies = {};
			Application.Data.Movies.Collection = new MovieCollection();
		},				

		mainApp: function(id) {
			console.log('rendering main....');
		        var layout = new Application.Template.AppMainLayout();
		        Application.main.show(layout);
	        	Application.main.currentView.header.show(new Application.Template.AppHeader());
		        Application.main.currentView.mainContent.show(new Application.Template.AppMainContent());
		        Application.main.currentView.notifications.show();
            
                Application.Data.Movies.CollectionView = new MovieCollectionView({Application:Application,collection:Application.Data.Movies.Collection});							
                Application.Data.Movies.CollectionView.render();
			}
		});

	return App_Router;
});


