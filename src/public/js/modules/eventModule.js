define('movieModule',
       ['movieModel', 'movieCollection', 'movieView', 'movieCollectionView'],
       function(MovieModel, MovieCollection, MovieView, MovieCollectionView){
	
	 var MovieModule = function(module){
 
	         module.Model = {Movie:MovieModel},
	         module.Collection = {Movies : MovieCollection},
	         module.View = {
        	        Movie:MovieView,
	                Collection:MovieCollectionView
	         },
	         module.Data = {};
	 };

	 return MovieModule;

});
