define('movieModel',['backbone', 'configuration'],function(Backbone, Configuration){

	var EventModel = Backbone.Model.extend({
		urlRoot: '/api/movies/:_id',
		defaults: {title: "Default Movie Title"}	
		});	
	return EventModel;
});
