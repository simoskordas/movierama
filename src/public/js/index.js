$(document).ready(function() {
        
    function getServerData(url, label) {
        $("#listDescription").html('Fetching movies list...');
        $("#moviesList").html('');        
        $.getJSON( url, function( data ) {
          var moviesAray = [];
          var allMoviesNumber = data.length;
          var allMoviesNumberStr = allMoviesNumber.toString();
          data.forEach(function( movieObject ) {
              movieObject['reviews'] = movieObject.reviewsNumberRotten + movieObject.reviewsNumberTheMovieDB;
                var movieBox = "<div style='border:1px solid Black; padding:10px; margin:5px; width:600px;'> \
                                   <div style='padding:5px;font-weight:bold;'>{{ title }}</div> \
                                   <div style='padding:10px;'>{{ year }} - Staring: {{ actors }}</div> \
                                   <div style='padding:10px;'>{{ description }}</div> \
                                   <div style='padding:10px; width:100%; alight:right;'>{{ reviews }} Reviews</div> \
                               </div>"
                var movieBoxHTML = Mustache.render(movieBox, movieObject);   
                moviesAray.push( movieBoxHTML);
          });
          $("#moviesList").html(moviesAray.join( "" ));
          $("#listDescription").html(allMoviesNumberStr + ' ' + label);
        });        
    }

    getServerData('/api/movies', 'movies in theaters this week');
    
    $("#doSearch").click(function() {
        var userInput = $('#searchBox').val();
        if (userInput != ''){
            getServerData('/api/movies/search?q=' + userInput, 'movies in theaters this week');
        }
    });    
    
});