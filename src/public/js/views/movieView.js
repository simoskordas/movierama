define('movieView',['backbone', 'handlebars', 'text!/js/templates/movieModule/movieViewTemplate.hbs'],function(Backbone, Handlebars, MovieViewTemplate){

	var MovieView = Backbone.View.extend({
		tagName: 'div',
		className: 'movie',
		render: function() {
			var template = Handlebars.compile(MovieViewTemplate);
			this.el.innerHTML = template(this.model.toJSON());
			return this;
			},
		});
	return MovieView;
})



