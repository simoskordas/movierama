define('movieCollectionView',['backbone','handlebars','text!/js/templates/movieModule/movieCollectionViewTemplate.hbs','movieView'],function(Backbone,Handlebars,Template,MovieView){

	var MovieCollectionView = Backbone.View.extend({
		template : Handlebars.compile(Template),
		el:'#moviesList',
		Application:'',
		initialize: function(options){
			Application = options.Application;
			this.collection.bind('add', this.addMovie, this);
		},
		addMovie: function(event){
			$(this.el).append(new MovieView({model: movie}).render().el);
		},
		render: function() {
			$(this.el).innerHTML = this.template();
			this.collection.fetch();
			return this;
		}
	});
	return MovieCollectionView;
})


