/*jslint node:true */
'use strict';

var mongoose = require('mongoose');  

var movieSchema = new mongoose.Schema({  
  title: String,
  description: String,
  year: Number,
  releaseDate: String,
  reviewsNumberRotten: Number,
  reviewsNumberTheMovieDB: Number,
  nowPlaying: Boolean,
  actors: [String]
});

//movieSchema.methods.toJSON = function(){
//    return {
//      title: this.title,
////      description: this.description,
//      year: this.year,
//      releaseDate: this.releaseDate,
//      reviewsNumberRotten: this.reviewsNumberRotten,
//      reviewsNumberTheMovieDB: this.reviewsNumberTheMovieDB,
////      actors: this.actors
//    };
//};
module.exports = mongoose.model('Movie', movieSchema);