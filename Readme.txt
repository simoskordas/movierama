------------------------------------------------------------------------------------------------------------------
Project setup (Ubuntu)
------------------------------------------------------------------------------------------------------------------
- Install npm
- Install mongoDB on default port (27017), create one database 'myapp' & one collection 'movies'. MovieRama does not use credentials to connect to MongoDB (default MongoDB settings).
- Checkout source code and on the same directory that package.json is stored run:
      
      npm install

  It should complete in 2-3 minutes (it does not do any system-wide installation).
  After this, from the same directory you should either run the tests:
      
      npm test
  
  or run the server:

      node src/index.js

 Verify that the GUI is available through browser:

      http://localhost:4000/index.html

 * all setting above (ports & db name)   can be changed in file: <MovirRma root>/src/core/config.js

