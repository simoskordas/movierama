process.env.NODE_ENV = process.env.NODE_ENV || 'test';
process.env.TEST_ENV = process.env.TEST_ENV || 'test';


describe('All tests', function() {
    
    describe('Generic Movies API', function() {
        var MoviesAPI  = require('../src/components/moviesAPI');
        var RestClient = require('restler'); 
        
        var listOfMoviesFromAPI = [];
        listOfMoviesFromAPI.push({title: 'title1',
                                  description: 'description1',
                                  year: 2000,
                                  releaseDate: '2000-01-01',
                                  reviews: 0,
                                  actors: ['actor1', 'actor2']});
        listOfMoviesFromAPI.push({title: 'title2',
                                  description: 'description2',
                                  year: 2001,
                                  releaseDate: '2001-01-01',
                                  reviews: 1,
                                  actors: ['actor3', 'actor4']});
        listOfMoviesFromAPI.push({title: 'title3',
                                  description: 'description3',
                                  year: 2002,
                                  releaseDate: '2002-01-01',
                                  reviews: 2,
                                  actors: ['actor5', 'actor6'] });   
        
        var listOfMoviesTransformedtoCommonMovieModels = [];
        listOfMoviesTransformedtoCommonMovieModels.push({title: 'title1',
                                          description: 'description1',
                                          year: 2000,
                                          releaseDate: '2000-01-01',
                                          reviewsNumberRotten: 0,
                                          reviewsNumberTheMovieDB: 0,
                                          actors: ['actor1', 'actor2']});
        listOfMoviesTransformedtoCommonMovieModels.push({title: 'title2',
                                          description: 'description2',
                                          year: 2001,
                                          releaseDate: '2001-01-01',
                                          reviewsNumberRotten: 1,                                               
                                          reviewsNumberTheMovieDB: 0,
                                          actors: ['actor3', 'actor4']});
        listOfMoviesTransformedtoCommonMovieModels.push({title: 'title3',
                                          description: 'description3',
                                          year: 2002,
                                          releaseDate: '2002-01-01',
                                          reviewsNumberRotten: 2,                                               
                                          reviewsNumberTheMovieDB: 0,
                                          actors: ['actor5', 'actor6'] });           
        
        it('Consume REST API', function() {
            var expectedAPIResponse = {'key': 'value'};
            RestClient.get = function() {
                return {
                  on: function(event, callback) {
                    callback(expectedAPIResponse, '');
                  }
                }
              };
            var moviesAPI = new MoviesAPI();
            return moviesAPI.getUrl('testurl').then(function(apiResponseData){
                assert.equal(expectedAPIResponse, apiResponseData) ; 
            });
        });
        
        it('Transform REST API movie results to common movie model', function() {
            var mockAPIResponse = {
                total:3,
                list: listOfMoviesFromAPI
            };
            var context = {
                 extractMoviesListFromAPIResults: function (apiResponse){
                     return apiResponse.list;
                 },
                 toCommonMovieModel: function (apiMovieObject){
                    return commonMovieObject = {  
                        title: apiMovieObject.title,
                        description: apiMovieObject.description,
                        year: apiMovieObject.year,
                        releaseDate: apiMovieObject.releaseDate,
                        reviewsNumberRotten: apiMovieObject.reviews,
                        reviewsNumberTheMovieDB: 0,
                        actors: apiMovieObject.actors
                    };
                 },
            };
            var testGetCommonMovieModels = (MoviesAPI.prototype.getCommonMovieModels).bind(context);
            var promiseGetCommonMovieModels= testGetCommonMovieModels(mockAPIResponse);
            return promiseGetCommonMovieModels.then(function(transformedMovieList) {
                    assert.deepEqual(transformedMovieList, listOfMoviesTransformedtoCommonMovieModels) ; 
            }); 
         }); 
        
         it('Get now playing movies', function() {
            var testUrl = 'http://testurl.com';
//            var testUrlAPIResponse = {'total': 3,
//                                       'list': listOfMoviesTransformedtoCommonMovieModels                  
//                                     };      
            var context = {
                 getNowPlayingUrl: function (){
                     return testUrl;
                 },
                 getUrl: function (url){
                    var commonModelsList = listOfMoviesTransformedtoCommonMovieModels;
                    var promise = new Promise(function (resolve, reject) {
                        resolve(commonModelsList);
                    });          
                    return promise;                     
                 },
                 getCommonMovieModels: function (apiResponse){
                    var commonModelsList = listOfMoviesTransformedtoCommonMovieModels;
                    var promise = new Promise(function (resolve, reject) {
                        resolve(commonModelsList);
                    });          
                    return promise;
                 }
            };

            var testGetNowPlayingMovies = (MoviesAPI.prototype.getNowPlayingMovies).bind(context);
            var promiseGetNowPlayingMovies = testGetNowPlayingMovies();
            return promiseGetNowPlayingMovies.then(function(commonMoviesObjectList) {
                assert.deepEqual(commonMoviesObjectList, listOfMoviesTransformedtoCommonMovieModels); 
            });              
         }); 
         it('Search movies', function() {
            var testUrl = 'http://testurl.com';
//            var testUrlAPIResponse = {'total': 3,
//                                       'list': listOfMoviesFromAPI                  
//                                     };  
            var testText = 'time';
            var context = {
                 searchMoviesUrl: function (){
                     return testUrl;
                 },
                 getUrl: function (url){
                    var commonModelsList = listOfMoviesTransformedtoCommonMovieModels;
                    var promise = new Promise(function (resolve, reject) {
                        resolve(commonModelsList);
                    });          
                    return promise;                     
                 },
                 getCommonMovieModels: function (apiResponse){
                    var commonModelsList = listOfMoviesTransformedtoCommonMovieModels;
                    var promise = new Promise(function (resolve, reject) {
                        resolve(commonModelsList);
                    });          
                    return promise;
                 }
            };

            var testSearchMovies = (MoviesAPI.prototype.searchMovies).bind(context);
            var promiseSearchMovies = testSearchMovies(testText);
            return promiseSearchMovies.then(function(commonMoviesObjectList) {
                assert.deepEqual(commonMoviesObjectList, listOfMoviesTransformedtoCommonMovieModels); 
            });              
         });         
        
         it('Test isDateValid', function() {
            assert.isArray(MoviesAPI.prototype.isDateValid('2016-01-01'));
            assert.isArray(MoviesAPI.prototype.isDateValid('1960-01-01'));
            assert.isArray(MoviesAPI.prototype.isDateValid('2050-12-12'));
            assert.isNull(MoviesAPI.prototype.isDateValid('2016-01-90'));
            assert.isNull(MoviesAPI.prototype.isDateValid('2016-01'));
            assert.isNull(MoviesAPI.prototype.isDateValid('text'));
         });
    });    
    
    describe('RottenTomatoes API', function() {
        var RottenTomatoesAPI  = require('../src/components/rottenTomatoesAPI');
        var RestClient = require('restler'); 
        it('Get movies list from API response', function(done) {

            var moviesListFromAPI = [{'key1':'val1'},{'key12':'val2'}];
            mock_api_response = {"total":90,
                                 "movies": moviesListFromAPI
                                };
            var moviesListExtracted = RottenTomatoesAPI.extractMoviesListFromAPIResults(mock_api_response);
            assert.equal(moviesListFromAPI, moviesListExtracted);
            done();
        });
        it('Get now playing url', function(done) {
            var expectedUrl = 'http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?apikey=qtqep7qydngcc7grk4r4hyd9'
            var urlReturned = RottenTomatoesAPI.getNowPlayingUrl();
            assert.equal(urlReturned, expectedUrl);
            done();
        });   
        it('Get movie reviews url', function(done) {
            var testMovieID = '100';
            var expectedUrl = 'http://api.rottentomatoes.com/api/public/v1.0/movies/' + testMovieID + '/reviews.json?apikey=qtqep7qydngcc7grk4r4hyd9'
            var urlReturned = RottenTomatoesAPI.getMovieReviewsUrl(testMovieID);
            assert.equal(urlReturned, expectedUrl);
            done();
        });
        it('Get search movies url', function(done) {
            var expectedUrl = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json?q=time&page_limit=50&page=1&apikey=qtqep7qydngcc7grk4r4hyd9'
            var urlReturned = RottenTomatoesAPI.searchMoviesUrl('time');
            assert.equal(urlReturned, expectedUrl);
            done();
        });         
        it('Get movie reviews number', function() {
            var mockMovieReviewsAPIResponse = {"total":55,"reviews":[]};
            RestClient.get = function() {
                return {
                  on: function(event, callback) {
                    callback(mockMovieReviewsAPIResponse, '');
                  }
                }
              };            
            var promiseGetMovieReviewsNumber = RottenTomatoesAPI.getMovieReviewsNumber(100);
            return promiseGetMovieReviewsNumber.then(function(reviewsNumber) {
                assert.equal(reviewsNumber, 55); 
            });              
        });         
        
        it('Transform API movie object to common movie model', function() {
            var rottenMovieFromAPI = {
                 "id":"771390242",
                 "title":"Deadpool",
                 "year":2016,
                 "mpaa_rating":"R",
                 "runtime":100,
                 "critics_consensus":"",
                 "release_dates":{
                    "theater":"2016-02-12"
                 },
                 "ratings":{
                    "critics_rating":"Certified Fresh",
                    "critics_score":83,
                    "audience_rating":"Upright",
                    "audience_score":94
                 },
                 "synopsis":"Based upon Marvel Comics",
                 "posters":{
                    "thumbnail":"http://resizing.flixster.com/qJhVYSeGbZJFaHc3QsdqDbV0Wzg=/54x80/v1.bTsxMTQyMDkxNDtqOzE2OTAzOzIwNDg7MTAwMDsxNDgw",
                    "profile":"http://resizing.flixster.com/qJhVYSeGbZJFaHc3QsdqDbV0Wzg=/54x80/v1.bTsxMTQyMDkxNDtqOzE2OTAzOzIwNDg7MTAwMDsxNDgw",
                    "detailed":"http://resizing.flixster.com/qJhVYSeGbZJFaHc3QsdqDbV0Wzg=/54x80/v1.bTsxMTQyMDkxNDtqOzE2OTAzOzIwNDg7MTAwMDsxNDgw",
                    "original":"http://resizing.flixster.com/qJhVYSeGbZJFaHc3QsdqDbV0Wzg=/54x80/v1.bTsxMTQyMDkxNDtqOzE2OTAzOzIwNDg7MTAwMDsxNDgw"
                 },
                 "abridged_cast":[
                    {  "name":"Ryan Reynolds",
                       "id":"162652367",
                       "characters":["Wade Wilson/Deadpool"]
                    },
                    {  "name":"Morena Baccarin",
                       "id":"381984430",
                       "characters":["Vanessa Carlysle/Copycat"]
                    },
                    {  "name":"Ed Skrein",
                       "id":"771439086",
                       "characters":["Ajax"]
                    }
                 ],
                 "alternate_ids":{
                    "imdb":"1431045"
                 },
                 "links":{
                    "self":"//api.rottentomatoes.com/api/public/v1.0/movies/771390242.json",
                    "alternate":"//www.rottentomatoes.com/m/deadpool/",
                    "cast":"//api.rottentomatoes.com/api/public/v1.0/movies/771390242/cast.json",
                    "reviews":"//api.rottentomatoes.com/api/public/v1.0/movies/771390242/reviews.json",
                    "similar":"//api.rottentomatoes.com/api/public/v1.0/movies/771390242/similar.json"
                 }
              };
            var mockMovieReviewsAPIResponse = {"total":237,"reviews":[]};
            RestClient.get = function() {
                return {
                  on: function(event, callback) {
                    callback(mockMovieReviewsAPIResponse, '');
                  }
                }
              };             
            var expectedCommonMovieModel = {
                title: "Deadpool",
                description: "Based upon Marvel Comics",
                year: 2016,
                releaseDate: "2016-02-12",
                reviewsNumberRotten: 237,
                reviewsNumberTheMovieDB: 0,
                nowPlaying: false,
                actors: ["Ryan Reynolds", "Morena Baccarin", "Ed Skrein"]             
            }
            var expectedReviewsAPIResponse = {"total":237,"reviews":[{}]};
            var promiseToCommonMovieModel = RottenTomatoesAPI.toCommonMovieModel(rottenMovieFromAPI);
            return promiseToCommonMovieModel.then(function(commonMovieObject) {
                assert.deepEqual(commonMovieObject, expectedCommonMovieModel);  
            });              
        });            
    });  
    
    describe('TheMovieDB API', function() {
        var TheMovieDBAPI  = require('../src/components/theMovieDbAPI');
        var RestClient = require('restler'); 
        it('Get movies list from API response', function(done) {

            var moviesListFromAPI = [{'key1':'val1'},{'key12':'val2'}];
            mock_api_response = {"page": 1,
                                 "results": moviesListFromAPI
                                };
            var moviesListExtracted = TheMovieDBAPI.extractMoviesListFromAPIResults(mock_api_response);
            assert.equal(moviesListFromAPI, moviesListExtracted);
            done();
        });
        it('Get now playing url', function(done) {
            var expectedUrl = 'http://api.themoviedb.org/3/movie/now_playing?api_key=186b266209c2da50f898b7977e2a44dd'
            var urlReturned = TheMovieDBAPI.getNowPlayingUrl();
            assert.equal(urlReturned, expectedUrl);
            done();
        }); 
        it('Get search movies url', function(done) {
            var expectedUrl = 'http://api.themoviedb.org/3/search/movie/?query=time&api_key=186b266209c2da50f898b7977e2a44dd'
            var urlReturned = TheMovieDBAPI.searchMoviesUrl('time');
            assert.equal(urlReturned, expectedUrl);
            done();
        }); 
        it('Get movie details url', function(done) {
            var expectedUrl = 'http://api.themoviedb.org/3/movie/345767344?api_key=186b266209c2da50f898b7977e2a44dd'
            var urlReturned = TheMovieDBAPI.movieDetailsUrl('345767344');
            assert.equal(urlReturned, expectedUrl);
            done();
        });         
        
        it('Transform API movie object to common movie model', function() {
            var movieFromAPI = {
                "poster_path": "/inVq3FRqcYIRl2la8iZikYYxFNR.jpg",
                "adult": false,
                "overview": "Based upon Marvel Comics",
                "release_date": "2016-02-09",
                "genre_ids": [12, 28, 35],
                "id": 293660,
                "original_title": "Deadpool",
                "original_language": "en",
                "title": "Deadpool",
                "backdrop_path": "/n1y094tVDFATSzkTnFxoGZ1qNsG.jpg",
                "popularity": 65.848775,
                "vote_count": 1364,
                "video": false,
                "vote_average": 7.33
            };
            var expectedCommonMovieModel = {
                title: "Deadpool",
                description: "Based upon Marvel Comics",
                year: 2016,
                releaseDate: "2016-02-09",
                reviewsNumberRotten: 0,
                reviewsNumberTheMovieDB: 1364,
                nowPlaying: false,
                actors: []
            };
            
            var promiseToCommonMovieModel = TheMovieDBAPI.toCommonMovieModel(movieFromAPI);
            return promiseToCommonMovieModel.then(function(commonMovieObject) {
                assert.deepEqual(commonMovieObject, expectedCommonMovieModel); 
            }); 
        });         
    });    
    
    describe('Movies Cache', function() {
                


    });       

})